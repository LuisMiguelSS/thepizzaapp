package io.luismiguelss.thepizzaapp.Models;

public class Ingredient {

    //
    // Attributes
    //
    private String name;
    private String image;

    //
    // Getters
    //
    public String getName() { return name; }
    public String getImage() { return image; }

    //
    // Constructors
    //
    public Ingredient() {
        this("Unknown","");
    }
    public Ingredient(String name, String image) {
        this.name = name;
        this.image = image;
    }

    @Override
    public boolean equals(Object other) {

        if(other != null && other instanceof Ingredient) {
            Ingredient ingr = (Ingredient)other;

            if(this.name.equals(ingr.name) &&
                this.image.equals(ingr.image))
                return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "name='" + name + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
