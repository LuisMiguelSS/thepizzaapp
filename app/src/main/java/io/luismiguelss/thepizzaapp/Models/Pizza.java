package io.luismiguelss.thepizzaapp.Models;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;

public class Pizza {

    //
    // Attributes
    //
    private String name;
    private double crust;
    private double cost;
    private ArrayList<Ingredient> ingredientList;
    public String imageUrl;
    private Drawable image;

    //
    // Constructor
    //
    public Pizza() {
        this("Unknown",1,0,new ArrayList<Ingredient>(), null, null);
    }
    public Pizza(String name, double crust, double cost, ArrayList<Ingredient> ingredients, String imageUrl, Drawable image) {
        this.name = name;
        this.crust = crust;
        this.cost = cost;
        this.ingredientList = ingredients;
        this.imageUrl = imageUrl;
        this.image = image;
    }

    //
    // Getters
    //
    public String getName() { return name; }
    public double getCrust() { return crust; }
    public double getCost() { return cost; }
    public ArrayList<Ingredient> getIngredients() { return ingredientList; }
    public String getImageUrl() { return imageUrl; }
    public Drawable getImage() { return  image; }

    //
    // Setters
    //
    public void setName(String name) {
        this.name = name;
    }
    public void setCrust(double crust) {
        this.crust = crust;
    }
    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredientList = ingredients;
    }
    public void setImage(Drawable image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", crust=" + crust +
                ", cost=" + cost +
                ", ingredientList=" + ingredientList +
                ", imageUrl='" + imageUrl + '\'' +
                ", image='" + image.toString() + '\'' +
                '}';
    }
}
