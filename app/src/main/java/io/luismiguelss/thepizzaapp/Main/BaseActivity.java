package io.luismiguelss.thepizzaapp.Main;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AppCompatActivity;
import java.util.Locale;

import io.luismiguelss.thepizzaapp.R;
import io.luismiguelss.thepizzaapp.Utils.AppManager;
import io.luismiguelss.thepizzaapp.Utils.AppManager.Locales;

public class BaseActivity extends AppCompatActivity {

    //
    // Attributes
    //
    protected static Locales currentLanguage = AppManager.DEFAULT_LANGUAGE;
    public int counterItemSelected = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        updateLanguage(currentLanguage);

    }

    //
    // Other methods
    //

    public void updateLanguage(Locales newLanguage) {

        if(newLanguage != null) {

            if (newLanguage != currentLanguage) {
                Resources res = this.getResources();

                // Change locale settings in the app.
                DisplayMetrics dm = res.getDisplayMetrics();
                Configuration conf = res.getConfiguration();

                Locale locale = new Locale(newLanguage.toString().toLowerCase());
                conf.setLocale(locale);
                Locale.setDefault(locale);

                res.updateConfiguration(conf, dm);

                refresh();
            }

            currentLanguage = newLanguage;
        }

    }
    public ArrayAdapter<String> createArrayAdapterByLang(AppManager.Locales currentLang) {

        // Replace Languages order
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, android.R.id.text1);

        if(currentLang == AppManager.Locales.ES) {
            adapter.add(getString(R.string.spanish));
            adapter.add(getString(R.string.english));

        } else {
            adapter.add(getString(R.string.english));
            adapter.add(getString(R.string.spanish));
        }

        return adapter;
    }

    public void refresh() {
        // recreate(); For SDK >= 11 (Optional)
        finish();
        startActivity(getIntent());
    }

}
