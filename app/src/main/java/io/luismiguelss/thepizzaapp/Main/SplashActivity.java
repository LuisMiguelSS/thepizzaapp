package io.luismiguelss.thepizzaapp.Main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;

import com.google.firebase.FirebaseApp;

import io.luismiguelss.thepizzaapp.Access.AccessActivity;
import io.luismiguelss.thepizzaapp.R;

/**
 * Animation from: https://github.com/domsu/UIAndroid/blob/master/app/src/main/java/com/uiandroid/examples/sample/SlideInOnAppStartActivity.java
 * Delay from: https://stackoverflow.com/questions/7965494/how-to-put-some-delay-in-calling-an-activity-from-another-activity
 * */

public class SplashActivity extends AppCompatActivity {

    // Attributes
    private boolean animated = false;
    private static final int START_DELAY = 300;
    private static final int DURATION_INITIAL = 600;
    private static final int DURATION_NEXT_VIEW_FACTOR = 30;
    private static final float INTERPOLATOR_FACTOR = 2f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        FirebaseApp.initializeApp(this);

        // Open LoginActivity after 1.6 seconds
        new Handler().postDelayed(() -> {
                startActivity( new Intent(SplashActivity.this, AccessActivity.class) );
                finish();
        }, 1600);

    }

    // Slide In Animation Methods
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus && !animated) {
            startSlideInAnimation();
            animated = true;
        }
    }

    private void startSlideInAnimation() {
        ViewGroup windowRoot = findViewById(android.R.id.content);
        ViewGroup contentRoot = (ViewGroup) windowRoot.getChildAt(0);

        for (int i = 0; i < contentRoot.getChildCount(); i++) {
            View v = contentRoot.getChildAt(i);

            animateSingleView(windowRoot, i, v);
        }
    }

    private void animateSingleView(ViewGroup windowRoot, int viewPosition, View view) {
        view.setTranslationY(windowRoot.getHeight());
        view.setAlpha(0);

        view.animate()
                .translationY(0)
                .alpha(1)
                .setStartDelay(START_DELAY)
                .setDuration(DURATION_INITIAL + DURATION_NEXT_VIEW_FACTOR * viewPosition)
                .setInterpolator(new DecelerateInterpolator(INTERPOLATOR_FACTOR)).start();
    }
}
