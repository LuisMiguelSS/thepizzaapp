package io.luismiguelss.thepizzaapp.Main;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Objects;

import io.luismiguelss.thepizzaapp.Access.LoginActivity;
import io.luismiguelss.thepizzaapp.Adapters.PizzaTuple;
import io.luismiguelss.thepizzaapp.Menu.*;
import io.luismiguelss.thepizzaapp.R;

public class MainPageActivity extends AppCompatActivity
                                implements  HomeFragment.OnFragmentInteractionListener,
                                            MenusFragment.OnFragmentInteractionListener ,
                                            CartFragment.OnFragmentInteractionListener {
    //
    // Attributes
    //
    public static ArrayList<PizzaTuple> cartList = new ArrayList<>();


    //
    // Other methods
    //
    private void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.containerFragments, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = null;
        if (connectManager != null) {
            activeNetworkInfo = connectManager.getActiveNetworkInfo();
        }

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    //
    // Events
    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainpage_activity);

        // Center ActionBar title
        Objects.requireNonNull(getSupportActionBar()).setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        // Network Connection error
        if(!isNetworkAvailable()) {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.errorNoConnectionTitle))
                    .setMessage(getString(R.string.errorNoConnectionBody))
                    .setNeutralButton(android.R.string.ok, (dialog, which) -> onBackPressed())
                    .setIcon(R.drawable.ic_signal_no_connection)
                    .setCancelable(false)
                    .show();
        }
        // Load HOME Fragment
        openFragment(HomeFragment.newInstance());

        // Set components
        //
        // Attributes
        //
        BottomNavigationView bottomNav = findViewById(R.id.bottomNav);
        bottomNav.setOnNavigationItemSelectedListener(menuItem -> {

            // Navigate to every fragment
            switch (menuItem.getItemId()) {
                case R.id.navigation_home:
                    openFragment(HomeFragment.newInstance());
                    return true;

                case R.id.navigation_order:
                    openFragment(MenusFragment.newInstance());
                    return true;

                case R.id.navigation_cart:
                    openFragment(CartFragment.newInstance());
                    return true;
            }
            return false;
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        finish();
        startActivity( new Intent(getApplicationContext(), LoginActivity.class) );
        overridePendingTransition(R.anim.slide_right,R.anim.slide_left);
    }
}
