package io.luismiguelss.thepizzaapp.Listeners;

import android.view.View;
import android.widget.AdapterView;

import io.luismiguelss.thepizzaapp.Main.BaseActivity;
import io.luismiguelss.thepizzaapp.Utils.AppManager;

public class SpinnerListener implements AdapterView.OnItemSelectedListener {

    //
    // Attributes
    //
    private BaseActivity currentActivity;

    //
    // Constructor
    //
    public SpinnerListener(BaseActivity activity) {
        this.currentActivity = activity;
    }

    // ITEM SELECTED
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // Get Spinner
        if(currentActivity != null && ++currentActivity.counterItemSelected > 1) { // This avoids being called when activity loads
            String selectedItem = parent.getSelectedItem().toString();

            if(selectedItem.equalsIgnoreCase("Inglés") || selectedItem.equalsIgnoreCase("English"))
                currentActivity.updateLanguage(AppManager.Locales.EN);
            else
                currentActivity.updateLanguage(AppManager.Locales.ES);
        }

    }

    // NOTHING SELECTED
    public void onNothingSelected(AdapterView<?> parent) {
    }

}
