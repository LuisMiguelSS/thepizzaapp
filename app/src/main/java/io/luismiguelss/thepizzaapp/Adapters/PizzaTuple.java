package io.luismiguelss.thepizzaapp.Adapters;

/**
 * This class represents a Tuple of < Pizza, count >
 *     where "Pizza" is the object and "Count" the number
 *     of it that the user has chosen.
 * */
public class PizzaTuple<Pizza, Count> {

    //
    // Attributes
    //
    private Pizza pizza;
    private Count count;

    //
    // Constructor
    //
    PizzaTuple(io.luismiguelss.thepizzaapp.Models.Pizza pizza, Integer count) {
        this.pizza = (Pizza) pizza;
        this.count = (Count) count;
    }

    //
    // Getters
    //
    public io.luismiguelss.thepizzaapp.Models.Pizza getPizza() {
        if (pizza instanceof io.luismiguelss.thepizzaapp.Models.Pizza)
            return (io.luismiguelss.thepizzaapp.Models.Pizza)pizza;

        return null;
    }
    Integer getCount() {
        if(count instanceof Integer)
            return (Integer)count;

        return 0;
    }

    //
    // Setters
    //
    public void setPizza(io.luismiguelss.thepizzaapp.Models.Pizza pizza) {
        this.pizza = (Pizza) pizza;
    }
    boolean increment() {
        if( getCount() < 15) {
            count = (Count) (Integer)((Integer)count + 1);
            return true;
        }
        else
            return false;
    }
    void decrement() {
        if( getCount() > 0) {
            count = (Count) (Integer)((Integer)count - 1);
        }
    }
}
