package io.luismiguelss.thepizzaapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import io.luismiguelss.thepizzaapp.Menu.CartFragment;
import io.luismiguelss.thepizzaapp.Models.Pizza;
import io.luismiguelss.thepizzaapp.R;

import static io.luismiguelss.thepizzaapp.Main.MainPageActivity.cartList;

public class CartAdapter extends ArrayAdapter<CartAdapter.ViewHolder> {

    //
    // Attributes
    //
    private ArrayList<PizzaTuple> pizzaList;
    private Context context;
    private CartFragment parentFragment;

    //
    // Constructors
    //
    public CartAdapter(Context context, ArrayList<PizzaTuple> pizzas, CartFragment parent) {
        super(context, R.layout.pizza_cart_item);

        this.pizzaList = pizzas;
        this.context = context;
        parentFragment = parent;
    }

    // Holder
    static class ViewHolder {
        // Attributes
        ImageView image;
        TextView title;
        TextView value;
        TextView pizzaCount;
        ImageButton subtract;
        ImageButton add;
    }

    //
    // Other methods
    //

    //
    // Listeners
    //
    @NotNull
    @Override
    public View getView(int position, View v, ViewGroup parent) {
        Pizza pizza = pizzaList.get(position).getPizza();

        final CartAdapter.ViewHolder viewHolder;

        if (v == null) {
            viewHolder = new CartAdapter.ViewHolder();
            v = LayoutInflater.from(getContext()).inflate(R.layout.pizza_cart_item, parent, false);

            viewHolder.image = v.findViewById(R.id.pizzaImage);
            viewHolder.title = v.findViewById(R.id.pizzaTitle);
            viewHolder.value = v.findViewById(R.id.pizzaValue);
            viewHolder.pizzaCount = v.findViewById(R.id.pizzaCount);
            viewHolder.subtract = v.findViewById(R.id.btnMinus);
            viewHolder.add = v.findViewById(R.id.btnPlus);


            v.setTag(viewHolder);

        } else
            viewHolder = (ViewHolder) v.getTag();

        viewHolder.image.setImageDrawable(pizza.getImage());
        viewHolder.title.setText(pizza.getName());
        viewHolder.value.setText(pizza.getCost() + " €");
        viewHolder.pizzaCount.setText(String.valueOf(pizzaList.get(position).getCount()));

        viewHolder.subtract.setOnClickListener( (View view) -> {
            if(viewHolder.pizzaCount.getText().toString().equals("1")) {
                pizzaList.remove(position);
                notifyDataSetChanged();
                parentFragment.CheckEmptyAdapter();
            }
            else{
                notifyDataSetChanged();
                cartList.get(position).decrement();
                viewHolder.pizzaCount.setText( String.valueOf(pizzaList.get(position).getCount()));
            }
        });

        viewHolder.add.setOnClickListener( (View view) -> {
            if(!viewHolder.pizzaCount.getText().toString().equals("15")) {
                if(cartList.get(position).increment())
                    viewHolder.pizzaCount.setText( String.valueOf(pizzaList.get(position).getCount()));
                else
                    Toast.makeText(context, context.getString(R.string.maxNumberPizzas), Toast.LENGTH_LONG).show();
            }
            else
                Toast.makeText(context, context.getString(R.string.maxNumberPizzas), Toast.LENGTH_LONG).show();
        });

        return v;
    }

    @Override
    public int getCount() {
        return pizzaList == null?  0 : pizzaList.size();
    }

}
