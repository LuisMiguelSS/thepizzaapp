package io.luismiguelss.thepizzaapp.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.tasks.Task;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import io.luismiguelss.thepizzaapp.Firebase.DatabaseManager;
import io.luismiguelss.thepizzaapp.Menu.popups.OwnPizzaDialog;
import io.luismiguelss.thepizzaapp.Models.Ingredient;
import io.luismiguelss.thepizzaapp.Models.Pizza;
import io.luismiguelss.thepizzaapp.R;

import static io.luismiguelss.thepizzaapp.Main.MainPageActivity.cartList;

public class PizzaAdapter extends RecyclerView.Adapter<PizzaAdapter.ViewHolder> {

    //
    // Attributes
    //
    private ArrayList<Pizza> pizzaList;
    private Context context;
    private final int VIEW_TYPE_FOOTER = R.layout.own_pizza_item;
    private final int VIEW_TYPE_CELL = R.layout.pizza_list_item;

    //
    // Constructors
    //
    public PizzaAdapter(ArrayList<Pizza> pizzas, Context context) {
        this.pizzaList = pizzas;
        this.context = context;
    }

    // Holder
    static class ViewHolder extends RecyclerView.ViewHolder {

        //
        // Attributes
        ImageView image;
        TextView title;
        TextView value;
        TextView ingredients;
        ImageView addPizzaBtn;

        // Own Pizza (Add)
        CardView addPizzaCard;

        //
        // Constructor
        ViewHolder(View v) {
            super(v);

            ingredients = null;
            addPizzaBtn = null;
            value = null;
            title = null;
            image = null;
            addPizzaCard = null;

            if(v.getTag() != null && v.getTag().toString().equals("OWNPIZZA"))
                addPizzaCard = v.findViewById(R.id.ownPizzaCard);
            else {
                image = v.findViewById(R.id.pizzaImage);
                title = v.findViewById(R.id.pizzaTitle);
                value = v.findViewById(R.id.pizzaValue);
                ingredients = v.findViewById(R.id.ingredients);
                addPizzaBtn = v.findViewById(R.id.addPizzaToCart);
            }
        }
    }

    //
    // Other methods
    //
    private boolean cartListContains(Pizza pizza) {
        for (PizzaTuple tuple: cartList){
            if(tuple.getPizza().getName().equalsIgnoreCase(pizza.getName()))
                return true;
        }

        return false;
    }
    private PizzaTuple getPizzaTupleByPizzaName(String name) {
        for (PizzaTuple tuple: cartList) {
            if(tuple.getPizza().getName().equalsIgnoreCase(name.trim()))
                return tuple;
        }

        return null;
    }

    //
    // Listeners
    //
    @NotNull
    @Override
    public PizzaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        context = parent.getContext();

        if(viewType == VIEW_TYPE_CELL)
            v = LayoutInflater.from(parent.getContext()).inflate(VIEW_TYPE_CELL,parent,false);
        else if(viewType == VIEW_TYPE_FOOTER)
            v = LayoutInflater.from(parent.getContext()).inflate(VIEW_TYPE_FOOTER,parent,false);

        return new ViewHolder(v);
    }

    // Add array items to list
    @Override
    public void onBindViewHolder(@NotNull final PizzaAdapter.ViewHolder holder, int position) {

        if(position == pizzaList.size()) {

            // Get ingredients
            DatabaseManager dbm = DatabaseManager.getInstanceOf();

            // Footer - add own pizza button
            FragmentTransaction fm = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
            OwnPizzaDialog editNameDialogFragment = OwnPizzaDialog.newInstance(dbm.getIngredientList());
            editNameDialogFragment.show(fm, "fragment_edit_name");
            //https://guides.codepath.com/android/using-dialogfragment

        } else { // Load pizza item
            final Pizza pizza = pizzaList.get(position);

            holder.title.setText(pizza.getName());
            holder.image.setImageDrawable(context.getDrawable(R.drawable.pizza_foreground)); // default pizza image
            holder.value.setText(pizza.getCost() + " €");

            // Ingredients
            holder.ingredients.setText(" " + context.getString(R.string.ingredients) + ":");

            for (Ingredient ingredient : pizza.getIngredients())
                holder.ingredients.append("\n   -" + ingredient.getName());

            // Add Pizza to cart
            holder.addPizzaBtn.setOnClickListener( (View v) -> {

                if(!cartListContains(pizza)) {
                    cartList.add(new PizzaTuple(pizza,1));
                    Toast.makeText(context, context.getString(R.string.pizzaAdded), Toast.LENGTH_SHORT).show();
                } else {
                    PizzaTuple tuple = getPizzaTupleByPizzaName(pizza.getName());

                    if(tuple != null)
                        if(!tuple.increment())
                            Toast.makeText(context, context.getString(R.string.maxNumberPizzas), Toast.LENGTH_SHORT).show();
                }
            });

            // Get pizza image
            DatabaseManager.getInstanceOf().getFirebaseStorage().child(pizza.getImageUrl()).getDownloadUrl().addOnCompleteListener( (@NonNull Task<Uri> task) -> {

                if(task.isSuccessful())
                    Glide.with(context)
                            .asDrawable()
                            .load(task.getResult())
                            .into(new CustomTarget<Drawable>() {
                                @Override
                                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                    pizzaList.get(position).setImage(resource);
                                    holder.image.setImageDrawable(pizza.getImage());
                                }

                                @Override
                                public void onLoadCleared(@Nullable Drawable placeholder) {}

                            });
            });
        }

    }

    @Override
    public int getItemCount() {
        return pizzaList == null?  0 : pizzaList.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == pizzaList.size()) ? VIEW_TYPE_FOOTER : VIEW_TYPE_CELL;
    }

}
