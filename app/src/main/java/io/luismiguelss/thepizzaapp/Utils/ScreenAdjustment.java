package io.luismiguelss.thepizzaapp.Utils;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.WindowManager;

public class ScreenAdjustment {

    public static void displayActivityAsDialog(Activity activity) {
        displayActivityAsDialog(activity,90,82);
    }

    public static void displayActivityAsDialog(Activity activity, double widthPercentage, double heightPercentage) {

        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        activity.getWindow().setLayout((int)(width * (widthPercentage/100) ), (int)(height * (heightPercentage/100) ));

        WindowManager.LayoutParams params = activity.getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = -20;

        activity.getWindow().setAttributes(params);
    }
}
