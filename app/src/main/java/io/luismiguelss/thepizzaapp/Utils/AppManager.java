package io.luismiguelss.thepizzaapp.Utils;

import java.util.Locale;

public class AppManager {

    //
    // Attributes
    //
    public static final int MAX_USERNAME_LENGTH = 16;
    public static final int MAX_EMAIL_LENGTH = 35;
    public static final int MAX_PASSWORD_LENGTH = 20;
    public static Locales DEFAULT_LANGUAGE = getLocaleByString(Locale.getDefault().getDisplayLanguage());
    public enum Locales {
        EN("English"),ES("Spanish");

        String language;
        Locales(String language) {
            this.language = language;
        }

        public String getLanguage() {return language;}

    }

    //
    // Other methods
    //
    public static Locales getLocaleByString(String language) {

        if(language != null) {
            String languageTranslated = language.equals("español") || language.equals("spanish") ? "spanish" : "english";

            for (Locales lang : Locales.values())
                if (lang.getLanguage().equalsIgnoreCase(languageTranslated))
                    return lang;
        }

        return null;
    }

}
