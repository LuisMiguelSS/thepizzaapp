package io.luismiguelss.thepizzaapp.Utils;

import android.view.View;

public class Animations {

    // SLIDE
    public static View slideDown(View v, int duration) {
        v.animate()
                .translationY(v.getHeight())
                .setDuration(duration);

        return v;
    }
    public static View slideUp(View v, int duration) {
        v.animate()
                .translationY(0)
                .setDuration(duration);

        return v;
    }

    // SHOW
    public static View hide(View v) {
        v.animate().alpha(0.0f);

        return v;
    }
    public static View show(View v) {
        v.animate().alpha(1.0f);

        return v;
    }
}
