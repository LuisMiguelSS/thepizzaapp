package io.luismiguelss.thepizzaapp.Utils;

import static io.luismiguelss.thepizzaapp.Utils.AppManager.MAX_PASSWORD_LENGTH;
import static io.luismiguelss.thepizzaapp.Utils.AppManager.MAX_USERNAME_LENGTH;

public class RegexEx {

    //
    // Regex Constants
    //
    public static String USERNAME = "^[\\w]{1," + MAX_USERNAME_LENGTH + "}$";
    public static String PASSWORD = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8," + MAX_PASSWORD_LENGTH + "}$";
    public static String EMAIL = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"; // According to RFC2822

}
