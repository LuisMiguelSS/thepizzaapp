package io.luismiguelss.thepizzaapp.Menu;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Objects;

import io.luismiguelss.thepizzaapp.Adapters.PizzaAdapter;
import io.luismiguelss.thepizzaapp.Firebase.DatabaseManager;
import io.luismiguelss.thepizzaapp.Models.Ingredient;
import io.luismiguelss.thepizzaapp.Models.Pizza;
import io.luismiguelss.thepizzaapp.R;

public class MenusFragment extends Fragment {

    //
    // Attributes
    //

    //View
    private RecyclerView pizzasRecyclerView;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefresh;

    //Firebase
    private static DatabaseManager dbm;
    private ArrayList<Pizza> pizzaList;

    //
    // Constructor (should be empty)
    //
    public MenusFragment() {
    }

    public static MenusFragment newInstance() {
        MenusFragment fragment = new MenusFragment();
        return fragment;
    }

    //
    // Other methods
    //
    public void toggleLoading() {
        if(pizzasRecyclerView.getVisibility() == View.VISIBLE) {
            pizzasRecyclerView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            pizzasRecyclerView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }


    //
    // Listeners
    //
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Firebase
        //dbm = DatabaseManager.getInstanceOf();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_menus, container, false);

        // Content Refresher
        swipeRefresh = v.findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeColors(getContext().getColor(R.color.green));
        swipeRefresh.setOnRefreshListener(() -> loadItems(true));

        // Recycler view
        pizzasRecyclerView = v.findViewById(R.id.pizzaListView);
        pizzasRecyclerView.setHasFixedSize(true);

        // Progress bar
        progressBar = v.findViewById(R.id.progressBar);

        loadItems(false);

        return v;
    }

    public void loadItems(boolean hideMainSpinner) {

        // Hide Recycler view and show loading bar
        if(!hideMainSpinner)
            toggleLoading();

        /*
            TODO: rework this way of retrieving data from Firebase. NOTE: it should use child and addListenerForSingleValue,
                as per "https://stackoverflow.com/questions/39620872/how-to-show-loading-message-while-fetching-data-from-firebase-database"
                question on StackOverflow, so that the first main progressbar takes the time that the app needs to get all the data
                and not just the x amount of seconds which is told in the Handler (see below, just before onAttach).

        FirebaseDatabase.getInstance().getReference().child("pizzas").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DocumentSnapshot doc : dataSnapshot.getRef().getDatabase().getReference().getDatabase()..getDocuments()) {

                    // Get Pizza
                    Pizza pizza = new Pizza(
                            doc.getString("nombre"),
                            doc.getDouble("masa"),
                            doc.getDouble("precio"),
                            new ArrayList<>(),
                            Objects.requireNonNull(doc.getDocumentReference("imagen")).getPath().replace("gs:/pizzeria-proyecto-android.appspot.com/",""),
                            null
                    );
                    pizzaList.add(pizza);


                    // Get ingredients
                    ((ArrayList<DocumentReference>) Objects.requireNonNull(doc.get("ingredientes")))
                            .forEach( documentReference -> documentReference.get().addOnCompleteListener( (@NonNull Task<DocumentSnapshot> task) -> {
                                ArrayList<Ingredient> pizzaIngredients = pizza.getIngredients();

                                Ingredient ingredient = new Ingredient(
                                        Objects.requireNonNull(task.getResult()).getString("nombre"),
                                        Objects.requireNonNull(task.getResult().getDocumentReference("imagen")).getPath().replace("gs:/pizzeria-proyecto-android.appspot.com/","")
                                );

                                pizzaIngredients.add(ingredient);
                            }));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/

        // Load pizzas
        CollectionReference pizzas = FirebaseFirestore.getInstance().collection("pizzas");


        pizzas.get().addOnSuccessListener( (QuerySnapshot queryDocumentSnapshots) -> {
            pizzaList = new ArrayList<>();

            for (DocumentSnapshot doc : queryDocumentSnapshots.getDocuments()) {

                // Get Pizza
                Pizza pizza = new Pizza(
                        doc.getString("nombre"),
                        doc.getDouble("masa"),
                        doc.getDouble("precio"),
                        new ArrayList<>(),
                        Objects.requireNonNull(doc.getDocumentReference("imagen")).getPath().replace("gs:/pizzeria-proyecto-android.appspot.com/",""),
                        null
                );
                pizzaList.add(pizza);


                // Get ingredients
                ((ArrayList<DocumentReference>) Objects.requireNonNull(doc.get("ingredientes")))
                        .forEach( documentReference -> documentReference.get().addOnCompleteListener( (@NonNull Task<DocumentSnapshot> task) -> {
                            ArrayList<Ingredient> pizzaIngredients = pizza.getIngredients();

                            Ingredient ingredient = new Ingredient(
                                    Objects.requireNonNull(task.getResult()).getString("nombre"),
                                    Objects.requireNonNull(task.getResult().getDocumentReference("imagen")).getPath().replace("gs:/pizzeria-proyecto-android.appspot.com/","")
                            );

                            pizzaIngredients.add(ingredient);
                        }));
            }


        });
        // Wait 2 seconds to load data from FireBase
        /*new Handler().postDelayed( () -> {
            if(dbm != null) {
                pizzasRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                PizzaAdapter pa = new PizzaAdapter(dbm.getPizzaList(),getActivity());
                pizzasRecyclerView.setAdapter(pa);

                if(!hideMainSpinner)
                    toggleLoading();

                swipeRefresh.setRefreshing(false);
            }
        }, 2000);*/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() { super.onDetach(); }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
