package io.luismiguelss.thepizzaapp.Menu;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import io.luismiguelss.thepizzaapp.Adapters.CartAdapter;
import io.luismiguelss.thepizzaapp.R;

import static io.luismiguelss.thepizzaapp.Main.MainPageActivity.cartList;

public class CartFragment extends Fragment {

    //
    // Attributes
    //
    private ListView pizzaListView;
    private OnFragmentInteractionListener mListener;
    private View inflatedView;

    //
    // Constructor
    //
    public CartFragment() {
    }

    public static CartFragment newInstance() {
        CartFragment fragment = new CartFragment();
        return fragment;
    }

    //
    // Other methods
    //
    public void CheckEmptyAdapter() {
        if(pizzaListView.getAdapter().getCount() > 0)
            inflatedView.findViewById(R.id.emptyCartContainer).setVisibility(View.GONE);
        else
            inflatedView.findViewById(R.id.emptyCartContainer).setVisibility(View.VISIBLE);
    }


    //
    // Listeners
    //
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        inflatedView = inflater.inflate(R.layout.fragment_cart,container,false);

        pizzaListView = inflatedView.findViewById(R.id.pizzaList);
        CartAdapter ca = new CartAdapter(getActivity(),cartList, this);
        pizzaListView.setAdapter(ca);

        CheckEmptyAdapter();


        return inflatedView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
    }
}
