package io.luismiguelss.thepizzaapp.Menu.popups;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.app.DialogFragment;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import java.util.ArrayList;

import io.luismiguelss.thepizzaapp.Models.Ingredient;
import io.luismiguelss.thepizzaapp.R;

public class OwnPizzaDialog extends DialogFragment {

    private LinearLayout ingredientsLayout;

    public OwnPizzaDialog() {
    }

    public static OwnPizzaDialog newInstance(ArrayList<Ingredient> ingredients) {

        ArrayList<String> ingredientsNames = new ArrayList<>();
        ingredients.forEach(ingredient -> ingredientsNames.add(ingredient.getName()));

        OwnPizzaDialog frag = new OwnPizzaDialog();
        Bundle args = new Bundle();
        args.putStringArrayList("ingredients", ingredientsNames);
        frag.setArguments(args);
        return frag;
    }


    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.own_pizza_dialog, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Reference view items
        ingredientsLayout = view.findViewById(R.id.layoutIngredients);

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        // Get ingredients
        ArrayList<String> ingredients = getArguments().getStringArrayList("ingredients");

        for (String ingredient : ingredients) {
            CheckBox cb = new CheckBox(null);
            cb.setText(ingredient);
            cb.setChecked(false);
            ingredientsLayout.addView(cb);
        }

    }

}