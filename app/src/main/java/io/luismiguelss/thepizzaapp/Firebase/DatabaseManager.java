package io.luismiguelss.thepizzaapp.Firebase;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.Executor;

import io.luismiguelss.thepizzaapp.Models.Ingredient;
import io.luismiguelss.thepizzaapp.Models.Pizza;

public class DatabaseManager {

    //
    // Attributes
    //
    private static DatabaseManager singleton;

    private FirebaseFirestore database = FirebaseFirestore.getInstance();
    private StorageReference firebaseStorage = FirebaseStorage.getInstance().getReference();

    private ArrayList<Pizza> pizzaList;
    private ArrayList<Ingredient> ingredientList;

    //
    // Getters
    //
    public ArrayList<Pizza> getPizzaList() { return pizzaList; }
    public ArrayList<Ingredient> getIngredientList() { return ingredientList; }
    public StorageReference getFirebaseStorage() { return firebaseStorage; }


    //
    // Constructors
    //
    private DatabaseManager() {

        //
        // GET PIZZAS
        CollectionReference pizzas = database.collection("pizzas");


        pizzas.get().addOnSuccessListener( (QuerySnapshot queryDocumentSnapshots) -> {
                pizzaList = new ArrayList<>();

                for (DocumentSnapshot doc : queryDocumentSnapshots.getDocuments()) {

                    // Get Pizza
                    Pizza pizza = new Pizza(
                            doc.getString("nombre"),
                            doc.getDouble("masa"),
                            doc.getDouble("precio"),
                            new ArrayList<>(),
                            Objects.requireNonNull(doc.getDocumentReference("imagen")).getPath().replace("gs:/pizzeria-proyecto-android.appspot.com/",""),
                            null
                    );
                    pizzaList.add(pizza);


                    // Get ingredients
                    ((ArrayList<DocumentReference>) Objects.requireNonNull(doc.get("ingredientes")))
                            .forEach( documentReference -> documentReference.get().addOnCompleteListener( (@NonNull Task<DocumentSnapshot> task) -> {
                                ArrayList<Ingredient> pizzaIngredients = pizza.getIngredients();

                                Ingredient ingredient = new Ingredient(
                                        Objects.requireNonNull(task.getResult()).getString("nombre"),
                                        Objects.requireNonNull(task.getResult().getDocumentReference("imagen")).getPath().replace("gs:/pizzeria-proyecto-android.appspot.com/","")
                                );

                                pizzaIngredients.add(ingredient);
                    }));
                }


        });

        //
        // GET INGREDIENTS
        CollectionReference ingredients = database.collection("ingredients");
        ingredients.get().addOnSuccessListener( (QuerySnapshot queryDocumentSnapshots) -> {

            ingredientList = new ArrayList<>();

            for (DocumentSnapshot doc : queryDocumentSnapshots.getDocuments()) {

                // Get Ingredient
                Ingredient ingredient = new Ingredient(
                        doc.getString("nombre"),
                        Objects.requireNonNull(doc.getDocumentReference("imagen")).getPath().replace("gs:/pizzeria-proyecto-android.appspot.com/","")
                );
                ingredientList.add(ingredient);
            }
        });
    }

    //
    // Other methods
    //
    public static DatabaseManager getInstanceOf() {
        if(singleton == null)
            singleton = new DatabaseManager();

        return singleton;
    }

}
