package io.luismiguelss.thepizzaapp.Access;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import io.luismiguelss.thepizzaapp.Main.BaseActivity;
import io.luismiguelss.thepizzaapp.R;


/**
 * This class works as background covering the Login and Register activities.
 * Extends from BaseActivity in order to be able to call the "currentLanguage" variable.
 */
public class AccessActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.access_activity);

        // Delay to show Login Activity
        new Handler().postDelayed(() -> {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
        }, 200);

    }
}
