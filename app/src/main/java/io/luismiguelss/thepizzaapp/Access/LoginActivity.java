package io.luismiguelss.thepizzaapp.Access;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.Serializable;
import java.util.Objects;

import io.luismiguelss.thepizzaapp.Main.BaseActivity;
import io.luismiguelss.thepizzaapp.Listeners.SpinnerListener;
import io.luismiguelss.thepizzaapp.Main.MainPageActivity;
import io.luismiguelss.thepizzaapp.R;
import io.luismiguelss.thepizzaapp.Utils.ScreenAdjustment;


public class LoginActivity extends BaseActivity implements Serializable {

    //
    // Attributes
    //
    private EditText username;
    private EditText password;
    private CheckBox loginAuto;
    private Spinner languageSpinner;
    private Button loginBtn;
    private TextView noAccount;

    //
    // onCreate
    //
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        // Styles adjustments
        ScreenAdjustment.displayActivityAsDialog(this);


        // Establish the Views from the activity
        initComponents();

        //
        // Spinner
        languageSpinner.setAdapter(createArrayAdapterByLang(currentLanguage));
        languageSpinner.setOnItemSelectedListener(new SpinnerListener(this));


        //
        // NoAccount TextView (Button functionality)
        noAccount.setOnClickListener( (View v) -> {
                finish();
                startActivity( new Intent(getApplicationContext(), RegisterActivity.class) );
                overridePendingTransition(R.anim.slide_up,R.anim.slide_down);
        });

        //
        // Login Btn
        loginBtn.setOnClickListener( (View v) -> {
                if(isLoginInputOK()) {

                    finish();
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), MainPageActivity.class));
                        overridePendingTransition(R.anim.slide_right,R.anim.slide_left);
                    }, 300);

                }
        });

        //
        // Checkbox LoginAuto
        loginAuto.setOnCheckedChangeListener( (CompoundButton buttonView, boolean isChecked) -> {
                if(isLoginInputOK()) {
                    if(loginAuto.isChecked())
                        disableAutoLoginAll(username.getText().toString());
                    else
                        disableAutoLoginAll(null);
                }
        });

        // Get user data if autologin is set to true
        Pair<String,String> storedLoginData = getStoredLoginData();
        if(storedLoginData != null) {
            username.setText(storedLoginData.first);
            password.setText(storedLoginData.second);

            loginAuto.setChecked(true);

        } else {
            loginAuto.setChecked(false);
            loginAuto.setEnabled(false);
        }

        // Check when password is entered if the user wants to store the login info
        password.setOnTouchListener((v, event) -> {
            loginAuto.setEnabled(true);
            return false;
        });

    }

    //
    // Other methods
    //
    public void initComponents() {
        languageSpinner = findViewById(R.id.languageSelector);
        loginBtn = findViewById(R.id.btnLogin);
        noAccount = findViewById(R.id.doNotHaveAccount);
        username = findViewById(R.id.fieldUsername);
        password = findViewById(R.id.fieldPasswd);
        loginAuto = findViewById(R.id.checkKeepLogInInfo);

        // Load login data if needed
    }

    public boolean isLoginInputOK() {

        // USERNAME
        if(username.getText() == null || username.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), getString(R.string.userNoEmpty), Toast.LENGTH_LONG).show();
            return false;
        }

        if(!new File(getApplicationInfo().dataDir, "shared_prefs/user_" + username.getText() + ".xml").exists()) {
            Toast.makeText(getApplicationContext(), getString(R.string.userNoExist), Toast.LENGTH_SHORT).show();
            return false;
        }


        // PASSWORD
        if(password.getText() == null || password.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), getString(R.string.passwdNoEmpty), Toast.LENGTH_LONG).show();
            return false;
        }

        if(!Objects.requireNonNull(getSharedPreferences("user_" + username.getText().toString().trim(), MODE_PRIVATE).getString("password", null))
                .equals(password.getText().toString())) {
            Toast.makeText(getApplicationContext(), getString(R.string.passwdNoMatch), Toast.LENGTH_SHORT).show();
            return false;
        }


        return true;
    }

    public Pair<String,String> getStoredLoginData() {
        File sharedPreferencesFolder = new File(getApplicationInfo().dataDir, "shared_prefs");

        if(sharedPreferencesFolder.exists() && sharedPreferencesFolder.isDirectory()) {

            //Get all XML shared preferences files
            for(String userFile: Objects.requireNonNull(sharedPreferencesFolder.list())) {
                SharedPreferences prefs = getApplicationContext().getSharedPreferences(userFile.substring(0,userFile.lastIndexOf('.'))
                        , Context.MODE_PRIVATE);
                if(prefs.contains("autologin")) {
                    if(prefs.getBoolean("autologin", false))
                        return new Pair<>(prefs.getString("username",null),prefs.getString("password",null));
                }
            }
            return null;
        }
        return null;
    }

    public void disableAutoLoginAll(@Nullable String userException) {
        File sharedPreferencesFolder = new File(getApplicationInfo().dataDir, "shared_prefs");

        if(sharedPreferencesFolder.exists() && sharedPreferencesFolder.isDirectory()) {

            //Get all XML shared preferences files
            for(String userFile : Objects.requireNonNull(sharedPreferencesFolder.list())) {
                SharedPreferences prefs = getApplicationContext().getSharedPreferences(userFile.substring(0,userFile.lastIndexOf('.'))
                        , Context.MODE_PRIVATE);

                // XML Editor
                SharedPreferences.Editor editor = prefs.edit();

                if(prefs.contains("autologin")) {

                    if(userException == null) {
                        editor.putBoolean("autologin",false);

                    } else {
                        // Check if username field is empty
                        if(!username.getText().toString().trim().equals("")) {

                            if(userFile.substring(userFile.indexOf("_")+1,userFile.lastIndexOf('.')).equals(username.getText().toString().trim()))
                                editor.putBoolean("autologin",true); // Enable THIS user
                            else
                                editor.putBoolean("autologin",false); // Disable

                        }
                    }


                } else
                    editor.putBoolean("autologin",false);

                editor.apply();

            }
        }
    }

    //
    // Listeners
    //

    // Quit App Dialog
    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.quitAppTitle))
                .setMessage(getString(R.string.quitAppBody))
                .setNegativeButton("No", null)
                .setPositiveButton(getString(R.string.yes), (dialog, which) -> {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);

                    startActivity(intent);
                })
                .show();

    }


}
