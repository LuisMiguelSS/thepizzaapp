package io.luismiguelss.thepizzaapp.Access;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Objects;

import io.luismiguelss.thepizzaapp.Main.BaseActivity;
import io.luismiguelss.thepizzaapp.Listeners.SpinnerListener;
import io.luismiguelss.thepizzaapp.R;
import io.luismiguelss.thepizzaapp.Utils.RegexEx;
import io.luismiguelss.thepizzaapp.Utils.ScreenAdjustment;

import static io.luismiguelss.thepizzaapp.Utils.AppManager.MAX_EMAIL_LENGTH;
import static io.luismiguelss.thepizzaapp.Utils.AppManager.MAX_PASSWORD_LENGTH;
import static io.luismiguelss.thepizzaapp.Utils.AppManager.MAX_USERNAME_LENGTH;

public class RegisterActivity extends BaseActivity {

    //
    // Atributos
    //
    private Spinner languageSpinner;
    private TextView btnAlreadyHaveAccount;
    private Button btnRegister;
    private EditText username;
    private EditText email;
    private EditText password_1;
    private EditText password_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);

        ScreenAdjustment.displayActivityAsDialog(this);


        // Establish the Views from the activity
        initComponents();

        //
        // Declare listeners
        btnAlreadyHaveAccount.setOnClickListener(v -> backToLogin());

        languageSpinner.setAdapter(createArrayAdapterByLang(currentLanguage));
        languageSpinner.setOnItemSelectedListener(new SpinnerListener(this));

        //
        // Register Btn
        btnRegister.setOnClickListener(v -> {

            if(registerUser()) {

                Toast.makeText(getApplicationContext(), getText(R.string.registerSuccess), Toast.LENGTH_LONG).show();
                new Handler().postDelayed(this::backToLogin, Toast.LENGTH_SHORT);

            }

        });
    }

    //
    // Other methods
    //
    public void initComponents() {
        languageSpinner = findViewById(R.id.languageSelector);
        btnAlreadyHaveAccount = findViewById(R.id.btnAlreadyHaveAccount);
        btnRegister = findViewById(R.id.btnRegister);
        username = findViewById(R.id.fieldUsername);
        email = findViewById(R.id.fieldEmail);
        password_1 = findViewById(R.id.fieldPasswd);
        password_2 = findViewById(R.id.fieldRepeatPasswd);
    }

    private boolean isInputOK() {

        // USERNAME
        if(username.getText() == null || username.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), getString(R.string.userNoEmpty), Toast.LENGTH_LONG).show();
            return false;
        }

        if (!username.getText().toString().trim().matches(RegexEx.USERNAME)){
            new androidx.appcompat.app.AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Username Input Incorrect")
                    .setMessage("The username should not contain spaces nor special characters.\n" +
                            " and has a maximum of " + MAX_USERNAME_LENGTH + " characters")
                    .setPositiveButton("Ok", (dialog, which) -> dialog.cancel())
                    .show();
            return false;
        }

        // EMAIL
        if(email.getText() == null || email.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "The email cannot be empty", Toast.LENGTH_LONG).show();
            return false;
        }

        if (email.getText() != null && !email.getText().toString().trim().matches(RegexEx.EMAIL)){
            new androidx.appcompat.app.AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Email Input Incorrect")
                    .setMessage("The email should not contain:\n" +
                            " - Special characters such as ( (, ), \\, ,)\n" +
                            " - up to " + MAX_EMAIL_LENGTH + " characters.")
                    .setPositiveButton("Ok", (dialog, which) -> dialog.cancel())
                    .show();
            return false;
        }

        // PASSWORD
        if(password_1.getText() == null || password_1.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), getString(R.string.passwdNoEmpty), Toast.LENGTH_LONG).show();
            return false;
        }

        if (password_1.getText() != null && !password_1.getText().toString().trim().matches(RegexEx.PASSWORD)){
            new androidx.appcompat.app.AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Password Input Incorrect")
                .setMessage("The password should contain:\n" +
                        " - at least 8 characters\n" +
                        " - up to " + MAX_PASSWORD_LENGTH + " characters\n" +
                        " - 1 uppercase, 1 lowercase and 1 number\n" +
                        " - (Optional) Special characters")
                .setPositiveButton("Ok", (dialog, which) -> dialog.cancel())
                .show();
            return false;
        }


        return true;
    }

    public boolean emailExists(String email) {
        File sharedPreferencesFolder = new File(getApplicationInfo().dataDir, "shared_prefs");

        if(sharedPreferencesFolder.exists() && sharedPreferencesFolder.isDirectory()) {

            //Get all XML shared preferences files
            for(String userFile: Objects.requireNonNull(sharedPreferencesFolder.list())) {
                SharedPreferences prefs = getApplicationContext().getSharedPreferences(userFile.substring(0,userFile.lastIndexOf('.'))
                                            , Context.MODE_PRIVATE);
                if(prefs.contains("email")) {
                    String emailValue = prefs.getString("email", null);

                    if (emailValue != null && emailValue.equalsIgnoreCase(email))
                        return true;
                }
            }
        }

        return false;
    }

    public boolean registerUser() {

        // Trim all inputs
        username.setText( username.getText().toString().trim() );
        email.setText( email.getText().toString().trim() );

        if( isInputOK() && password_1.getText().toString().equals(password_2.getText().toString()))
        {
            SharedPreferences prefs = getSharedPreferences("user_" + username.getText().toString().trim(),MODE_PRIVATE);

            if( prefs.contains("username") ) {
                Toast.makeText(getApplicationContext(),"User already exists", Toast.LENGTH_LONG).show();
                return false;
            } else if (emailExists(email.getText().toString())) {
                Toast.makeText(getApplicationContext(),"This email has already been used.", Toast.LENGTH_LONG).show();
                return false;
            } else
            {

                SharedPreferences.Editor editor = prefs.edit();

                editor.putString("username", username.getText().toString());
                editor.putString("email", email.getText().toString());
                editor.putString("password",password_1.getText().toString());
                editor.putBoolean("autologin",false);
                editor.apply();
            }

            return true;

        }
        else if ( !password_1.getText().toString().equals(password_2.getText().toString()) )
        {
            Toast.makeText(RegisterActivity.this,"Passwords don't match",Toast.LENGTH_LONG).show();
            return false;

        } else
            return false;
    }

    //
    // Listeners
    //
    public void backToLogin() {
        finish();

        startActivity( new Intent(getApplicationContext(), LoginActivity.class) );
        overridePendingTransition(R.anim.slide_up,R.anim.slide_down);
    }

    @Override
    public void onBackPressed() {
        backToLogin();
    }
}
